﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TaricTest.Model;
using TaricTest.Services;

namespace TaricTest
{
    /// <summary>
    /// Lógica de interacción para MovieHome.xaml
    /// </summary>
    public partial class MovieHome : Page
    {
        int current_page;
        double total_pages;
        int current_total_results;
        string current_search;
        string current_type;
        string current_movie_id;
        OmdbService service;

        public MovieHome()
        {
            InitializeComponent();
            paginationSearch.Visibility = Visibility.Collapsed; // Se esconde hasta realizar una busqueda
            service = new OmdbService();
        }

        // Control para la busqueda de una pelicula o serie
        private void Button_Search(object sender, RoutedEventArgs e)
        {
            string s = this.SearchTermTextBox.Text;
            string type = this.ComboBox1.Text;
            switch (type)
            {
                case "Pelicula":
                    type = "movie";
                    break;
                case "Serie":
                    type = "series";
                    break;
                default:
                    Console.WriteLine("No se especifico el tipo correcto");
                    break;
            }
            current_page = 1;
            MovieSearch result = service.GetMovieSearch(s, type, current_page);
            if (result.Response == "True")
            {
                moviesSearch.ItemsSource = result.Search;
                current_total_results = int.Parse(result.TotalResults);
                double total_pages_aux = current_total_results / 10.0;
                total_pages = Math.Ceiling(total_pages_aux);
                currentPageText.Text = current_page.ToString();
                lastPageText.Text = total_pages.ToString();

                current_search = s;
                current_type = type;
                paginationSearch.Visibility = Visibility.Visible;
                previousPage.IsEnabled = false;
                beginPage.IsEnabled = false;
            }
            else
            {
                if (result.Error == "Too many results.")
                {
                    MessageBoxResult message = MessageBox.Show("Error: Too many results. Intenta con una busqueda mas especifica", "Confirmation");
                }
                if (result.Error == "Movie not found!")
                {
                    MessageBoxResult message = MessageBox.Show("Movie not found!. No se encontro esa pelicula", "Confirmation");
                }
            }
        }

        // Control para ver el detalle de una pelicula o serie 
        private void SelectionMovie(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Movie selected_movie = (Movie)moviesSearch.SelectedItem;
                current_movie_id = selected_movie.imdbID;
            }
            catch
            {
                Console.WriteLine("Se perdio la refencia del objeto seleccionado");
            }
            Movie movie = service.GetMovieDetail(current_movie_id);
            titleSelectedMovie.Text = movie.Title + " (" + movie.Year + ")";
            ratedSelectedMovie.Text = "Rated: " + movie.Rated;
            runtimeSelectedMovie.Text = "Runtime: " + movie.Runtime;
            genreSelectedMovie.Text = "Genre: " + movie.Genre;
            if (movie.Poster == "N/A")
            {
                BitmapImage bitmapImage = new BitmapImage(new Uri("https://via.placeholder.com/200?text=Imagen+no+disponible"));
                posterSelectedMovie.Source = bitmapImage;
            }
            else
            {
                posterSelectedMovie.Source = new BitmapImage(new Uri(movie.Poster));
            }
            if (movie.Plot == "N/A")
            {
                plotSelectedMovie.Text = "No hay descripcion para mostrar. ";
            }
            else
            {
                plotSelectedMovie.Text = movie.Plot;
            }
            imdbRatingSelectedMovie.Text = movie.imdbRating + "/10\n" + movie.imdbVotes;
            star.Source = new BitmapImage(new Uri("https://iconsplace.com/wp-content/uploads/_icons/000000/256/png/rating-star-icon-256.png"));
            string rating_sources = "";
            foreach (Rating rating in movie.Ratings)
            {
                rating_sources = rating_sources + rating.Source + ": " + rating.Value + " ";
            }
            ratingSelectedMovie.Text = rating_sources;
            directorSelectedMovie.Text = "Director: " + movie.Director;
            writerSelectedMovie.Text = "Writer: " + movie.Writer;
            actorsSelectedMovie.Text = "Actors: " + movie.Actors;
            languageSelectedMovie.Text = "Language: " + movie.Language;
            countrySelectedMovie.Text = "Country: " + movie.Country;
            productionSelectedMovie.Text = "Production: " + movie.Production;
            websiteSelectedMovie.Text = "Website: " + movie.Website;
            boxOfficeSelectedMovie.Text = "Box office: " + movie.BoxOffice;
            releaseSelectedMovie.Text = "Released: " + movie.Released;

        }

        /// Controles para la paginacion
        private void Button_Next_Page(object sender, RoutedEventArgs e)
        {
            moviesSearch.SelectedItems.Clear();
            current_page += 1;
            currentPageText.Text = current_page.ToString();
            MovieSearch result = service.GetMovieSearch(current_search, current_type, current_page);
            if (result.Response == "True")
            {
                moviesSearch.ItemsSource = result.Search;
                previousPage.IsEnabled = true;
                beginPage.IsEnabled = true;
                if (current_page  == Convert.ToInt32(total_pages))
                {
                    nextPage.IsEnabled = false;
                    lastPage.IsEnabled = false;
                }
            }
            else
            {
                if (result.Error == "Too many results.")
                {
                    MessageBoxResult message = MessageBox.Show("Error: Too many results. Intenta con una busqueda mas especifica", "Confirmation");
                }
            }

        }

        private void Button_Previous_Page(object sender, RoutedEventArgs e)
        {
            current_page -= 1;

            currentPageText.Text = current_page.ToString();
            MovieSearch result = service.GetMovieSearch(current_search, current_type, current_page);
            if (result.Response == "True")
            {
                moviesSearch.ItemsSource = result.Search;
                nextPage.IsEnabled = true;
                lastPage.IsEnabled = true;
                if (current_page == 1)
                {
                    previousPage.IsEnabled = false;
                    beginPage.IsEnabled = false;
                }
            }
            else
            {
                if (result.Error == "Too many results.")
                {
                    MessageBoxResult message = MessageBox.Show("Error: Too many results. Intenta con una busqueda mas especifica", "Confirmation");
                }
            }

        }

        private void Button_Last_Page(object sender, RoutedEventArgs e)
        {
            current_page = Convert.ToInt32(total_pages);

            currentPageText.Text = current_page.ToString();
            MovieSearch result = service.GetMovieSearch(current_search, current_type, current_page);
            if (result.Response == "True")
            {
                moviesSearch.ItemsSource = result.Search;
                previousPage.IsEnabled = true;
                beginPage.IsEnabled = true;
                nextPage.IsEnabled = false;
                lastPage.IsEnabled = false;
            }
            else
            {
                if (result.Error == "Too many results.")
                {
                    MessageBoxResult message = MessageBox.Show("Error: Too many results. Intenta con una busqueda mas especifica", "Confirmation");
                }
            }

        }

        private void Button_Begin_Page(object sender, RoutedEventArgs e)
        {
            current_page = 1;

            currentPageText.Text = current_page.ToString();
            MovieSearch result = service.GetMovieSearch(current_search, current_type, current_page);
            if (result.Response == "True")
            {
                moviesSearch.ItemsSource = result.Search;
                previousPage.IsEnabled = false;
                beginPage.IsEnabled = false;
                nextPage.IsEnabled = true;
                lastPage.IsEnabled = true;
            }
            else
            {
                if (result.Error == "Too many results.")
                {
                    MessageBoxResult message = MessageBox.Show("Error: Too many results. Intenta con una busqueda mas especifica", "Confirmation");
                }
            }

        }

    }
}
