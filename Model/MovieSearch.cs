﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaricTest.Model
{
    /// <summary>
    /// Representa el objeto devuelto al realizar la busqueda
    /// </summary>
    class MovieSearch 
    {
        // Listado de las peliculas el limite son 10
        [JsonProperty(PropertyName = "search")]
        public List<Movie> Search { get; set; }

        // Total de resultados encontrados en la busqueda
        [JsonProperty(PropertyName = "totalResults")]
        public string TotalResults { get; set; }

        // Estado de la respuesta. Verdadero si se realizo con exito la busqueda. Falso si hubo un error
        [JsonProperty(PropertyName = "Response")]
        public string Response { get; set; }

        // En caso de que la respuesta sea Falsa el objeto presenta este campo
        [JsonProperty(PropertyName = "Error")]
        public string Error { get; set; }

    }
}
