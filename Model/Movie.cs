﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaricTest.Model
{
    /// <summary>
    /// Representa el objeto de Ratings
    /// </summary>
    class Rating
    {
        [JsonProperty(PropertyName = "Source")]
        public string Source { get; set; }

        [JsonProperty(PropertyName = "Value")]
        public string Value { get; set; }
    }

    /// <summary>
    /// Representa el objeto devuelto al pedir el detalle de una pelicula o serie
    /// </summary>
    class Movie
    {
        private string poster;

        [JsonProperty(PropertyName = "Title")]
        public string Title { get; set; }

        [JsonProperty(PropertyName = "Year")]
        public string Year { get; set; }

        [JsonProperty(PropertyName = "Rated")]
        public string Rated { get; set; }

        [JsonProperty(PropertyName = "Released")]
        public string Released { get; set; }

        [JsonProperty(PropertyName = "Runtime")]
        public string Runtime { get; set; }

        [JsonProperty(PropertyName = "Genre")]
        public string Genre { get; set; }

        [JsonProperty(PropertyName = "Director")]
        public string Director { get; set; }

        [JsonProperty(PropertyName = "Writer")]
        public string Writer { get; set; }

        [JsonProperty(PropertyName = "Actors")]
        public string Actors { get; set; }

        [JsonProperty(PropertyName = "Plot")]
        public string Plot { get; set; }

        [JsonProperty(PropertyName = "Language")]
        public string Language { get; set; }

        [JsonProperty(PropertyName = "Country")]
        public string Country { get; set; }

        [JsonProperty(PropertyName = "Awards")]
        public string Awards { get; set; }

        [JsonProperty(PropertyName = "Poster")]
        public string Poster
        {
            get
            {
                return poster;
            }
            set
            {
                if (value == "N/A")
                {
                    poster = "https://via.placeholder.com/200?text=Imagen+no+disponible";
                }
                else
                {
                    poster = value;
                }


            }
        }

        [JsonProperty(PropertyName = "Ratings")]
        public List<Rating> Ratings { get; set; }

        [JsonProperty(PropertyName = "Metascore")]
        public string Metascore { get; set; }

        [JsonProperty(PropertyName = "imdbRating")]
        public string imdbRating { get; set; }

        [JsonProperty(PropertyName = "imdbVotes")]
        public string imdbVotes { get; set; }

        [JsonProperty(PropertyName = "imdbID")]
        public string imdbID { get; set; }

        [JsonProperty(PropertyName = "Type")]
        public string Type { get; set; }

        [JsonProperty(PropertyName = "DVD")]
        public string DVD { get; set; }

        [JsonProperty(PropertyName = "BoxOffice")]
        public string BoxOffice { get; set; }

        [JsonProperty(PropertyName = "Production")]
        public string Production { get; set; }

        [JsonProperty(PropertyName = "Website")]
        public string Website { get; set; }

        [JsonProperty(PropertyName = "Response")]
        public string Response { get; set; }

        [JsonProperty(PropertyName = "Error")]
        public string Error { get; set; }
    }
}
