﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaricTest.Model;

namespace TaricTest.Services
{
    //Servicio utilizado para la comunicacion con la API proporcionada por omdb
    class OmdbService
    {
        private string Apikey = "162e058e";
        private string Path = "http://www.omdbapi.com";
        private RestClient client;

        public OmdbService()
        {
            client = new RestClient(Path);
        }

        // Peticion de busqueda
        public MovieSearch GetMovieSearch(string s, string type, int page)
        {
            var request = new RestRequest(Method.GET);
            request.AddParameter("apikey", Apikey);
            request.AddParameter("s", s);
            request.AddParameter("type", type);
            request.AddParameter("page", page.ToString());
            var response = client.Execute<MovieSearch>(request);
            MovieSearch result = new MovieSearch();
            result = response.Data;
            return result;
        }

        // Peticion de detalle
        public Movie GetMovieDetail(string i)
        {
            var request = new RestRequest(Method.GET);
            request.AddParameter("apikey", "162e058e");
            request.AddParameter("i", i);
            request.AddParameter("plot", "full");

            var response = client.Execute<Movie>(request);
            Movie result = new Movie();
            result = response.Data;
            return result;
        }



    }
}
